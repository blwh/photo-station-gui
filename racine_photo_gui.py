# RACINE PHOTO GUI
#
# This code is a photo gui written for Judith Felten's research group
# at UPSC during the course Design-Build-Test HT18. 
# The gui works as a unit together with a DSLR-camera and a samba storage
# unit to 1) take pictures, 2) scan pictures for QR-codes and 3) upload
# the images to the server.
# A more detailed explanation and usage manual can be found in the project
# manual written during the project course.

# More information can be found in the repository
# https://gitlab.com/blwh/photo-station-gui

# Written by: Lucas Hedstrom <hedstrom.lucas@gmail.om>

# FOr error logging
import logging
# GUI interface
import tkinter as tk
from tkinter import messagebox
from tkinter import ttk
# Opening files from computer
from tkinter.filedialog import askopenfilename, askopenfilenames
# To handle paths unspecific to OS
import ntpath
# used for the qr-handling
from pyzbar.pyzbar import decode
from pyzbar.pyzbar import ZBarSymbol
import cv2
# For more image handling
from PIL import Image
# used for directory browsing stuff
import os
# for easy date arithmetics
import datetime
import time
# For running scripts
import subprocess
# For parsing command-line args
import argparse

# Application class
class App(tk.Tk):
    # Dimension for the different frames
    app_dim = (700, 380)
    top_dim = (app_dim[0], 40)
    base_dim = (app_dim[0], app_dim[1] - top_dim[1])

    def __init__(self):
        tk.Tk.__init__(self)
        self.title('RACINE imaging controller')
        self._frame = None
        self.switch_frame(MainPage)
        # Style for buttons
        ttk.Style().configure('TButton')
        # Places menu
        Menu(self).place(anchor = tk.NW,
                         x = 0, y = 0,
                         width = App.top_dim[0], 
                         height = App.top_dim[1])
        Menu(self).configure(background='#e6e6e6')


    # Switching between frames (only one is used in this project)
    def switch_frame(self, frame_class):
        """Destroys current frame and replaces it with a new one."""
        new_frame = frame_class(self)
        if self._frame is not None:
            self._frame.destroy()
        self._frame = new_frame
        self._frame.place(anchor = tk.NW,
                          x = 0, y = App.top_dim[1] + 1,
                          width = App.base_dim[0],
                          height = App.base_dim[1])

# The text menu at the top of the GUI
class Menu(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master, bg = '#e6e6e6')
        # Variables for clock text
        self.time1 = ''
        self.time2 = time.strftime('%H:%M')
        self.clock = tk.Label(self, bg = '#e6e6e6', fg = '#404040', 
                              font=('courier', 16, 'bold'))
        self.clock.place(x = App.top_dim[0] - 75, y = 5)
        # Calls on the tick function
        self.tick()

        # Title on top of program
        app_title = tk.Label(self, bg = '#e6e6e6', fg = '#404040',
                            font=('courier', 16, 'bold'),
                            text = "Imaging Controller - RACINE")
        app_title.place(x = 5, y = 5)

    def tick(self):
        # get the current local time from the PC
        if (self.time2.find(':') == -1):
            self.time2 = time.strftime('%H:%M')
        else:
            self.time2 = time.strftime('%H %M')
        # if time string has changed, update it
        if self.time2 != self.time1:
            self.time1 = self.time2
            self.clock.config(text=self.time2)
        # calls itself every 1000 milliseconds
        # to update the time display as needed
        self.clock.after(1000, self.tick)

# The main (and only) page for the GUI
class MainPage(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master)

        # Standard button width and height
        b_width = 120; b_height = 34;
        # Brightness factor for QR scanning
        self.brt = 40
        self.scale = 0.5

        # Paths for images taken (input_dir) and scanned images,
        # ready for upload (output_dir) and samba mount
        self.input_dir = os.path.join(os.getcwd(), '', 'file_input')
        self.output_dir = os.path.join(os.getcwd(), '', 'file_output', '')
        self.mount_dir = os.path.join(os.getcwd(), '', 'mnt')
        self.storage_dir = ''

        # Create directories if not existent
        if not os.path.exists(self.input_dir):
            os.makedirs(self.input_dir)
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)
        if not os.path.exists(self.mount_dir):
            os.makedirs(self.mount_dir)

        # Button for taking images
        b_take_picture = ttk.Button(self,
                                    text = "Take picture",
                                    style = "TButton",
                                    command = lambda: self.take_image()) \
                            .place(anchor = tk.NW,
                                   x = 1, y = 1,
                                   height = b_height, width = b_width)
        # Button to initialize QR scanning
        b_qr_scan = ttk.Button(self,
                               text = "Scan QR",
                               style = "TButton",
                               command = \
                        lambda: self.scan_images(int(self.e_brt.get()))) \
                       .place(anchor = tk.NW,
                              x = b_width + 5, y = 1,
                              height = b_height, width = b_width)
        # Explanatory texts for brightness setting
        brt_title = tk.Label(self, fg = 'black',
                                     font=('courier', 12),
                                     text = "Brightness")
        brt_title.place(x = b_width + 5, y = b_height + 1)
        # Input for brightness. Locked but can be changed with buttons
        self.e_brt = ttk.Entry(self)
        self.e_brt.insert(0, self.brt)
        self.e_brt.config(state = tk.DISABLED)
        self.e_brt.place(anchor = tk.NW,
                         x = b_width + 5, y = b_height + 21,
                         height = b_height, width = b_width*1/3)
        # Buttons to increment and decrement brightness
        b_increase_brt = ttk.Button(self,
                                    text = "+", style = "TButton",
                                    command = \
                                    lambda: self.change_brightness(1)) \
                            .place(anchor = tk.NW, 
                                   x = b_width + 10 + b_width*1/3, 
                                   y = b_height + 21,
                                   height = b_height, width = b_height)
        b_decrease_brt = ttk.Button(self,
                                    text = "-", style = "TButton",
                                    command = \
                                    lambda: self.change_brightness(-1)) \
                            .place(anchor = tk.NW, 
                                   x = b_width + 10 + b_width*1/3 + b_height, 
                                   y = b_height + 21,
                                   height = b_height, width = b_height)
        # Explanatory texts for scale setting
        scale_title = tk.Label(self, fg = 'black',
                                     font=('courier', 12),
                                     text = "Scale")
        scale_title.place(x = b_width + 5, y = 2*b_height + 23)
        # Input for scale. Locked but can be changed with buttons
        self.e_scale = ttk.Entry(self)
        self.e_scale.insert(0, self.scale)
        self.e_scale.config(state = tk.DISABLED)
        self.e_scale.place(anchor = tk.NW,
                         x = b_width + 5, y = 2*b_height + 42,
                         height = b_height, width = b_width*1/3)
        # Buttons to increment and decrement scale
        b_increase_scale = ttk.Button(self,
                                    text = "+", style = "TButton",
                                    command = \
                                    lambda: self.change_scale(0.1)) \
                            .place(anchor = tk.NW, 
                                   x = b_width + 10 + b_width*1/3, 
                                   y = 2*b_height + 42,
                                   height = b_height, width = b_height)
        b_decrease_scale = ttk.Button(self,
                                    text = "-", style = "TButton",
                                    command = \
                                    lambda: self.change_scale(-0.1)) \
                            .place(anchor = tk.NW, 
                                   x = b_width + 10 + b_width*1/3 + b_height, 
                                   y = 2*b_height + 42,
                                   height = b_height, width = b_height)
        # Button to update lists
        b_update_lists = ttk.Button(self,
                                    text = "Update lists", 
                                    style = "TButton",
                                    command = lambda: self.update_lists()) \
                            .place(anchor = tk.NW, 
                                   x = App.base_dim[0] - 3*b_width - 2, 
                                   y = App.base_dim[1] - b_height - 5,
                                   height = b_height, width = b_width)
        # Button to upload images from scanned list
        b_create_folder = ttk.Button(self,
                                     text = "Create folder",
                                     style ="TButton",
                                     command = lambda: self.create_folder()) \
                             .place(anchor = tk.NW,
                                    x = 1, y = b_height*2 + 2,
                                    height = b_height, width = b_width)
        # Button to upload images from scanned list
        b_upload_images = ttk.Button(self,
                                     text = "Upload",
                                     style ="TButton",
                                     command = lambda: self.upload_images()) \
                             .place(anchor = tk.NW,
                                    x = 1, y = b_height*3 + 3,
                                    height = b_height, width = b_width)
        # Button to preview chosen image
        b_preview_image = ttk.Button(self,
                                     text = "Preview",
                                     style ="TButton",
                                     command = lambda: self.show_image()) \
                             .place(anchor = tk.NW,
                                    x = App.base_dim[0] - b_width, 
                                    y = App.base_dim[1] - b_height - 5,
                                    height = b_height, width = b_width)
        # Button to remove chosen image
        b_remove_image = ttk.Button(self,
                                    text = "Remove",
                                    style ="TButton",
                                    command = lambda: self.remove_image()) \
                            .place(anchor = tk.NW,
                                   x = App.base_dim[0] - 2*b_width - 1, 
                                   y = App.base_dim[1] - b_height - 5,
                                   height = b_height, width = b_width)
        # Button to connect to image server
        b_connect_server = ttk.Button(self,
                                      text = "Connect",
                                      style ="TButton",
                                      command = lambda: self.connect_server()) \
                              .place(anchor = tk.NW,
                                     x = 1, y = b_height*1 + 1,
                                     height = b_height, width = b_width)
        # Explanatory texts for list boxes
        taken_image_title = tk.Label(self, fg = 'black',
                                     font=('courier', 16),
                            text = "Taken images")
        taken_image_title.place(x = App.base_dim[0] - 402, y = 0)
        scanned_image_title = tk.Label(self, fg = 'black',
                                       font=('courier', 16),
                                       text = "Scanned images")
        scanned_image_title.place(x = App.base_dim[0] - 201, y = 0)
        # List for images taken
        self.taken_image_list = tk.Listbox(self, 
                                           selectmode = tk.SINGLE,
                                           font = ('', 10))
        self.taken_image_list.place(anchor = tk.NW,
                                    x = App.base_dim[0] - 402, y = 20,
                                    width = 200,
                                    height = \
                                    App.base_dim[1] - (b_height + 30))
        # List for images scanned
        self.scanned_image_list = tk.Listbox(self, 
                                             selectmode = tk.SINGLE,
                                             font = ('', 10))
        self.scanned_image_list.place(anchor = tk.NW,
                                      x = App.base_dim[0] - 201, y = 20,
                                      width = 200,
                                      height = \
                                      App.base_dim[1] - (b_height + 30))

        # Update the contents in the lists
        self.update_lists()
                                
    # Updates the varible that holds the brightness factor
    # by change.
    # Updates the entry containing this variable.
    def change_brightness(self, change):
        # Fix range on the amount of brt
        if ((self.brt < 100 and change > 0) \
                or (self.brt > -100 and change < 0)):
            self.brt = self.brt + change
            # Unlock entry
            self.e_brt.config(state = tk.NORMAL)
            self.e_brt.delete(0, tk.END)
            self.e_brt.insert(0, self.brt)
            # Lock entry
            self.e_brt.config(state = tk.DISABLED)

    # Updates the variable that holds the scale factor
    # by change.
    # Updates the entry containing this variable.
    def change_scale(self, change):
        # Fix range on the amount of scale
        if ((self.scale < 1 and change > 0) \
                or (self.scale > 0.15 and change < 0)):
            self.scale = self.scale + change
            # Unlock entry
            self.e_scale.config(state = tk.NORMAL)
            self.e_scale.delete(0, tk.END)
            self.e_scale.insert(0, self.scale)
            # Lock entry
            self.e_scale.config(state = tk.DISABLED)

    # Updates the content in the two lists
    def update_lists(self):
        # Empty the lists
        self.taken_image_list.delete(0,tk.END)
        self.scanned_image_list.delete(0,tk.END)
        # Fills the lists with content from predefined paths
        for dirpath, _, filenames in os.walk(self.input_dir):
            for filename in filenames:
                self.taken_image_list.insert('end', filename)
        for dirpath, _, filenames in os.walk(self.output_dir):
            for filename in filenames:
                self.scanned_image_list.insert('end', filename)

    # Shows a chosen image from one of the lists using
    # predefined os image preview software
    def show_image(self):
        # The if-else is to check which (if any) of the lists
        # has a chosen image
        if (self.taken_image_list.curselection()):
            # Retrieve image path
            file_dir = os.path.join(self.input_dir, \
        self.taken_image_list.get(self.taken_image_list.curselection()))
            # Open image
            img = Image.open(file_dir)
            width, height = img.size
            img = img.resize((720, int(720*height/width)), Image.ANTIALIAS) 
            img.show()
        elif (self.scanned_image_list.curselection()):
            # Retrieve image path
            file_dir = os.path.join(self.output_dir, \
        self.scanned_image_list.get(self.scanned_image_list.curselection()))
            # Open image
            img = Image.open(file_dir)
            width, height = img.size
            img = img.resize((720, int(720*height/width)), Image.ANTIALIAS) 
            img.show()
        else:
            messagebox.showinfo("Error", "No image selected")

    # Removes a chosen image from one of the lists using
    # similar method as in show_image
    def remove_image(self):
        # The if-else is to check which (if any) of the lists
        # has a chosen image
        if (self.taken_image_list.curselection()):
            # Retrieve image path
            file_dir = os.path.join(self.input_dir, \
        self.taken_image_list.get(self.taken_image_list.curselection()))
            # Remove image
            os.remove(file_dir)
        elif (self.scanned_image_list.curselection()):
            # Retrieve image path
            file_dir = os.path.join(self.output_dir, \
        self.scanned_image_list.get(self.scanned_image_list.curselection()))
            # Remove image
            os.remove(file_dir)
        else:
            messagebox.showinfo("Error", "No image selected")

        self.update_lists()
        app.update()

    # Takes an image using the supplied shell script
    def take_image(self):
        # grab the current timestamp and use it to construct the
        # output path
        ts = datetime.datetime.now()
        filename = "{}.png".format(ts.strftime("%Y-%m-%d_%H-%M-%S"))
        p = os.path.sep.join((self.input_dir, filename))

        # Exits gphoto processes. Otherwise throws a busy usb error
        subprocess.call(['pgrep -f gphoto | xargs kill -9'], shell=True)

        # save the file
        try:
            # Takes a photo with gphoto2
            subprocess.check_call(['gphoto2 --capture-image-and-download --filename=' + p], shell=True)
        except subprocess.CalledProcessError as e:
            logging.error('Something went wrong when taking an image. Check that the camera is on and/or correctly plugged in.')

        self.update_lists()
    
    # COnnects to the samba server using given script
    def connect_server(self):
        # Assume mounting goes as planned
        message = "Successfully mounted to microscopy server"

        try:
            a = subprocess.check_call(['sudo mount -t cifs -o username=%s,password=%s %s %s' % \
                    (username, password, host, self.mount_dir)], shell=True)
        except subprocess.CalledProcessError as e:
            logging.error('Something went wrong when mounting the server folder. This can occur if the folder is already monted. Check your host, username and password. Otherwise, restart the unit.')
            message = "Something went wrong when mounting to the server." \
                    + " Check the error log for more information."

        messagebox.showinfo("Server mount", message)
    
    # Creates a folder on the mounted server
    def create_folder(self):
        # If the folder is not mounted
        if not (os.system('mount | grep %s' % (self.mount_dir))):
            # grab the current timestamp and use it to construct the
            # output folder
            ts = datetime.datetime.now()
            dirname = "{}".format(ts.strftime("%Y-%m-%d_%H-%M-%S"))
            self.storage_dir = os.path.sep.join((self.mount_dir, dirname))

            os.system('sudo mkdir %s' % (self.storage_dir))
            messagebox.showinfo("Created folder", "Successfully created folder %s on the microscopy server." % (dirname))
        else:
            messagebox.showinfo("Image upload", \
                    "ERROR: Server not mounted.")

    # Upload images to the server in a dated folder
    def upload_images(self):
        # If there is a storage folder
        if self.storage_dir:
            # If the folder is not mounted
            if not (os.system('mount | grep %s' % (self.mount_dir))):
                # For all files in the path
                for dirpath, _, filenames in os.walk(self.output_dir):
                    for filename in filenames:
                        file_dir = os.path.join(dirpath, filename)
                        os.system('sudo mv %s %s' % \
                            (file_dir, os.path.join(self.storage_dir, filename)))
            else:
                messagebox.showinfo("Image upload", \
                        "ERROR: Server not mounted.")
        else:
            messagebox.showinfo("Image upload", \
                "ERROR: You have not created a folder.")

        self.update_lists()

    # Scans images for QR-codes and renames them
    def scan_images(self, brt):
        scan = messagebox.askquestion("QR-scan", \
                "Initiate scanning?")
        if (scan == "yes"):
            move = messagebox.askquestion("QR-scan", \
                "Do you want to move images that fails the scanning routine?")
            # filepath for the output files, 
            # i.e the files with their updated names according to the qr-info
            directory = os.fsencode(self.input_dir)
            nr_of_unnamed_files = 0
            # Loops over all files in directory
            for file in os.listdir(directory):
                # Get the filename
                filename_initial = os.fsdecode(file)
                print('Converting file ' + filename_initial)
                
                file_dir = str(os.path.join(directory, file),'utf-8')

                # opens the image as grayscale
                img = cv2.cv2.imread(file_dir, cv2.cv2.IMREAD_GRAYSCALE)
                img = cv2.resize(img, (0,0), fx=self.scale, fy=self.scale)

                # increase brightness of image
                # value could be + or - for brightness or darkness
                # change any value in the 2D list < max limit
                img[img < 255-brt] += brt  

                # convert to binary
                thresh = 100
                img_bn = cv2.cv2.threshold(img, \
                        thresh, 255, cv2.cv2.THRESH_BINARY)[1]

                # temp is a datatype containing info from the qr
                temp = decode(img_bn)

                if not temp:
                    print('The scan was unsuccessful')
                    if (move == "yes"):
                        os.rename(file_dir, \
                                self.output_dir + filename_initial)
                    nr_of_unnamed_files += 1
                else:
                    print('The scan was successful')
                    # temp[0][0] contains the string or data that the qr holds,
                    # the 8 last chars of this string is the date of inoculation
                    name = str(temp[0][0],'utf-8')

                    print(name)

                    # year of inoculation
                    qr_YYYY = int(name[-8:-4])
                    # month of inoculation
                    qr_MM = int(name[-4:-2])
                    # day of inoculation
                    qr_DD = int(name[-2:])
                    # converts the "integer date" to a date-datatype 
                    # for easy calculations
                    qr_date = datetime.date(qr_YYYY,qr_MM,qr_DD)

                    # gets todays date
                    today = datetime.date.today()

                    # calculate difference in days
                    days_since_inoculation =(today - qr_date).days

                    # new filename contains the info from the qr-code except 
                    # the last eight chars
                    # which was the date, instead this is replaced with days 
                    # since inoculation
                    filename = name[:-8] + str(days_since_inoculation) + '.png'

                    if name != 0 :
                        # moves and renames the file to correct 
                        # name in the output directory
                        os.rename(file_dir, self.output_dir + filename)
                    
                self.update_lists()
                app.update()

            print(nr_of_unnamed_files, \
                    "images could not be correctly renamed.\n")

            if (nr_of_unnamed_files == 1):
                donestring = \
                    "Scanning complete. " + str(nr_of_unnamed_files) + \
                    " image was not successfully scanned. " + \
                    "Please adjust the brightess factor or retake the image."
            elif (nr_of_unnamed_files == 0):
                donestring = "Scanning complete."
            else:
                donestring = \
                    "Scanning complete. " + str(nr_of_unnamed_files) + \
                    " images were not successfully scanned. " + \
                    "Please adjust the brightess factor or retake the images."

            messagebox.showinfo("Scan complete", donestring)

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-l", "--log", required=True,
            help="path to error log output")
    ap.add_argument("-s", "--server", required=True,
            help="name of samba server to mount to")
    ap.add_argument("-u", "--user", required=True,
            help="username to connect to samba server")
    ap.add_argument("-p", "--pass", required=True,
            help="pass to samba server")
    args = vars(ap.parse_args())

    # Not perfect, but global on the samba server information
    global username, password, host
    username = args['user']
    password = args['pass']
    host = args['server']

    # Configuration for error logging
    logging.basicConfig(filename=args["log"], level=logging.ERROR, 
                format='%(asctime)s %(levelname)s %(name)s %(message)s')
    #logging.basicConfig(filename="/home/pi/Desktop/epic", level=logging.ERROR, 
    #            format='%(asctime)s %(levelname)s %(name)s %(message)s')


    # Initialize the app
    app = App()
    # Window dimensions
    size = str(App.app_dim[0]) + "x" + str(App.app_dim[1])
    app.geometry(size)
    # Runs the app
    app.mainloop()
