## Description

This is a basic GUI developed for the RACINE-project during the course 
Design-Build-Test HT18 at Umeå University. The main goal of the project was to develop
a system to automatically detect and identify different root types on pictures
of plant cuts. More information about the project can be read in the project report
found in this repository.

This GUI serves as an intermediary to simplify the photography and storage of
many plates with plant cuts.
* Interface and take pictures using a USB-connected DSLR-camera
* Scan the images for QR-codes and rename based on the information found
* Upload to a cloud-storage

More detailed information can be read in the manual found in this repository.

## Installation

This program runs on Python3.

Start by cloning this repository
```sh
git clone https://gitlab.com/blwh/photo-station-gui
```

A few programs are required to use/configure the software,
* Gphoto2 interfaces with the DSLR-camera
* tkinter is the python module the UI is written in
* pip3 installs python3 packages using simple commands

These packages are simply installed by running
```sh
sudo apt-get install gphoto2 python3-tk python3-pip
```

Then we require the python packages pyzbar, opencv and pillow.
These are simply installed by
```sh
sudo pip3 install pyzbar opencv-python pillow
```

Now everything should be installed correctly. Simply run the program 
(AS ADMINISTRATOR!) by
```sh
sudo python3 racine_photo_gui.py
```

gphoto2
sudo apt-get install libgphoto2-dev pkg-config